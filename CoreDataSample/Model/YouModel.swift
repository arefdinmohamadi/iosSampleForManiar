//
//  YouModel.swift
//  CoreDataSample
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import Foundation


class YouModel {
    var name : String?
    var id : Int64?
    
    init() {
        
    }
    init(id : Int64? , name : String?) {
        self.name = name
        self.id = id
    }
}
