//
//  ShowRecordsController.swift
//  CoreDataSample
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import UIKit

class ShowRecordsController: UIViewController , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var tblView: UITableView!
    var youIs = [YouModel]()
    var idDelete : Int64?
    static var idEdite : Int64?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchAllRecords()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchAllRecords()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clicked(_ sender: UIButton) {
      self.idDelete = Int64(sender.tag+1)
      getDeleteCase()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return youIs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.lblName.text = self.youIs[indexPath.row].name
        cell.btnEdite.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        return cell
    }
    
    
    func fetchAllRecords () {
        let manager = DBManager.init()
        manager.selectAllCases { (info, error) in
            if info != nil , error == nil {
                self.youIs = info!
                self.tblView.reloadData()
            }else{
                print("error darim")
            }
        }
    }
    
    func getDeleteCase () {
        print("id \(idDelete!)")
        for item in youIs {
            self.idDelete = item.id
        }
        
        let manager = DBManager.init()
        manager.deleteCase(entityID: idDelete!) { (info, error) in
            
        }
        self.fetchAllRecords()
    }
    @IBAction func editeClicked(_ sender: UIButton) {
        ShowRecordsController.idEdite = Int64(sender.tag+1)
    }
}
