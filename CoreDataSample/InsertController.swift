//
//  InsertController.swift
//  CoreDataSample
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import UIKit

class InsertController: UIViewController {

    var youIs = [YouModel]()
    
    @IBOutlet weak var txtInsert: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func insertClicked(_ sender: UIButton) {
      
        
        let manager = DBManager.init()
        
        manager.insertNewCase(newEntity: YouModel.init(id: 1 , name: self.txtInsert.text!)) { (info, error) in
            if error != nil {
                print("error save!")
            }
        }
    }
    
}
