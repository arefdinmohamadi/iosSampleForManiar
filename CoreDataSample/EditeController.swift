//
//  EditeController.swift
//  CoreDataSample
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import UIKit

class EditeController: UIViewController {

    var selectedForEdite = YouModel()
    
    @IBOutlet weak var txtEdite: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

       self.getEdite()
        
        self.txtEdite.text = selectedForEdite.name
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clicked(_ sender: UIButton) {
        let manageer = DBManager.init()
        manageer.updateCase(newObj: YouModel.init(id: self.selectedForEdite.id, name: txtEdite.text!)) { (result, error) in
            if error != nil {
                print("error Darim!")
            }
        }
    }

    
    func getEdite() {
        let manager = DBManager.init()
        manager.selectAllCases { (info, error) in
            if info != nil , error == nil {
                for item in info! {
                    if item.id == ShowRecordsController.idEdite {
                        self.selectedForEdite = item
                    }
                }
            }
        }
    }
}
